<?php


namespace App\Service;


use App\Entity\Station;
use App\Repository\BookingRepository;

class BookingService
{
    protected BookingRepository $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function getDashboardCalendarByStation(Station $station)
    {
        return $this->bookingRepository->getDashboardCalendarByStation($station);
    }

}