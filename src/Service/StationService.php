<?php


namespace App\Service;


use App\Repository\BookingRepository;
use App\Repository\StationRepository;

class StationService
{
    protected StationRepository $stationRepository;
    protected BookingRepository $bookingRepository;

    public function __construct(
        StationRepository $stationRepository,
        BookingRepository $bookingRepository
    )
    {
        $this->stationRepository = $stationRepository;
        $this->bookingRepository = $bookingRepository;
    }

    public function findAll()
    {
        return $this->stationRepository->findAll();
    }

}