<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"station:read", "booking:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"station:read", "station:write", "booking:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Campervan::class, mappedBy="currentStation")
     * @Groups({"station:read", "station:write"})
     */
    private $campervans;

    /**
     * @ORM\OneToMany(targetEntity=EquipmentStock::class, mappedBy="station")
     * @Groups({"station:read", "station:write"})
     */
    private $equipmentStocks;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="startStation")
     */
    private $bookings;

    public function __construct()
    {
        $this->campervans = new ArrayCollection();
        $this->equipmentStocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Campervan[]
     */
    public function getCampervans(): Collection
    {
        return $this->campervans;
    }

    public function addCampervan(Campervan $campervan): self
    {
        if (!$this->campervans->contains($campervan)) {
            $this->campervans[] = $campervan;
            $campervan->setCurrentStation($this);
        }

        return $this;
    }

    public function removeCampervan(Campervan $campervan): self
    {
        if ($this->campervans->removeElement($campervan)) {
            // set the owning side to null (unless already changed)
            if ($campervan->getCurrentStation() === $this) {
                $campervan->setCurrentStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EquipmentStock[]
     */
    public function getEquipmentStocks(): Collection
    {
        return $this->equipmentStocks;
    }

    public function addEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if (!$this->equipmentStocks->contains($equipmentStock)) {
            $this->equipmentStocks[] = $equipmentStock;
            $equipmentStock->setStation($this);
        }

        return $this;
    }

    public function removeEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if ($this->equipmentStocks->removeElement($equipmentStock)) {
            // set the owning side to null (unless already changed)
            if ($equipmentStock->getStation() === $this) {
                $equipmentStock->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setStartStation($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getStartStation() === $this) {
                $booking->setStartStation(null);
            }
        }

        return $this;
    }
}
