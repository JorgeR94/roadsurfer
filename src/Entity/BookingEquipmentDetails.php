<?php

namespace App\Entity;

use App\Repository\BookingEquipmentDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BookingEquipmentDetailsRepository::class)
 */
class BookingEquipmentDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"booking:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Booking::class, inversedBy="equipmentDetails")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:write"})
     */
    private $booking;

    /**
     * @ORM\ManyToOne(targetEntity=EquipmentStock::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:read", "booking:write"})
     */
    private $equipment;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"booking:read", "booking:write"})
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(?Booking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

    public function getEquipment(): ?EquipmentStock
    {
        return $this->equipment;
    }

    public function setEquipment(?EquipmentStock $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
