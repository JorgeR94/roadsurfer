<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 */
class Equipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"equipment:read", "booking:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"equipment:read", "equipment:write", "booking:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=EquipmentStock::class, mappedBy="equipment")
     * @Groups({"equipment:read", "equipment:write"})
     */
    private $equipmentStocks;

    public function __construct()
    {
        $this->equipmentStocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|EquipmentStock[]
     */
    public function getEquipmentStocks(): Collection
    {
        return $this->equipmentStocks;
    }

    public function addEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if (!$this->equipmentStocks->contains($equipmentStock)) {
            $this->equipmentStocks[] = $equipmentStock;
            $equipmentStock->setEquipment($this);
        }

        return $this;
    }

    public function removeEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if ($this->equipmentStocks->removeElement($equipmentStock)) {
            // set the owning side to null (unless already changed)
            if ($equipmentStock->getEquipment() === $this) {
                $equipmentStock->setEquipment(null);
            }
        }

        return $this;
    }
}
