<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookingRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"booking:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Campervan::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:read", "booking:write"})
     */
    private $campervan;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:read", "booking:write"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:read", "booking:write"})
     */
    private $startStation;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:read", "booking:write"})
     */
    private $endStation;

    /**
     * @ORM\Column(type="date")
     * @Groups({"booking:read", "booking:write"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"booking:read", "booking:write"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"booking:read", "booking:write"})
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=BookingEquipmentDetails::class, mappedBy="booking", orphanRemoval=true, cascade={"persist"})
     * @Groups("post")
     * @Groups({"booking:read", "booking:write"})
     */
    private $equipmentDetails;

    public function __construct()
    {
        $this->equipmentDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getStartStation(): ?Station
    {
        return $this->startStation;
    }

    public function setStartStation(?Station $startStation): self
    {
        $this->startStation = $startStation;

        return $this;
    }

    public function getEndStation(): ?Station
    {
        return $this->endStation;
    }

    public function setEndStation(?Station $endStation): self
    {
        $this->endStation = $endStation;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|BookingEquipmentDetails[]
     */
    public function getEquipmentDetails(): Collection
    {
        return $this->equipmentDetails;
    }

    public function addEquipmentDetail(BookingEquipmentDetails $equipmentDetail): self
    {
        if (!$this->equipmentDetails->contains($equipmentDetail)) {
            $this->equipmentDetails[] = $equipmentDetail;
            $equipmentDetail->setBooking($this);
        }

        return $this;
    }

    public function removeEquipmentDetail(BookingEquipmentDetails $equipmentDetail): self
    {
        if ($this->equipmentDetails->removeElement($equipmentDetail)) {
            // set the owning side to null (unless already changed)
            if ($equipmentDetail->getBooking() === $this) {
                $equipmentDetail->setBooking(null);
            }
        }

        return $this;
    }
}
