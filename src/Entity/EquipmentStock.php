<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EquipmentStockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=EquipmentStockRepository::class)
 */
class EquipmentStock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"equipmentstock:read", "booking:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="equipmentStocks")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"equipmentstock:read", "equipmentstock:write", "booking:read"})
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity=Equipment::class, inversedBy="equipmentStocks")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"equipmentstock:read", "equipmentstock:write", "booking:read"})
     */
    private $equipment;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"equipmentstock:read", "equipmentstock:write", "booking:read"})
     */
    private $stock;

    /**
     * @ORM\OneToMany(targetEntity=BookingEquipmentDetails::class, mappedBy="equipment", orphanRemoval=true)
     * @Groups({"equipmentstock:read"})
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection|BookingEquipmentDetails[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(BookingEquipmentDetails $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setEquipment($this);
        }

        return $this;
    }

    public function removeBooking(BookingEquipmentDetails $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getEquipment() === $this) {
                $booking->setEquipment(null);
            }
        }

        return $this;
    }
}
