<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CampervanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CampervanRepository::class)
 */
class Campervan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"campervan:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"campervan:read", "campervan:write"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="campervans")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"campervan:read", "campervan:write"})
     */
    private $currentStation;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="campervan")
     * @Groups({"campervan:read"})
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCurrentStation(): ?Station
    {
        return $this->currentStation;
    }

    public function setCurrentStation(?Station $currentStation): self
    {
        $this->currentStation = $currentStation;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setCampervan($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getCampervan() === $this) {
                $booking->setCampervan(null);
            }
        }

        return $this;
    }
}
