<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function getDashboardCalendarByStation(Station $station)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();
        $builder->select('b.startDate,
               s.name as station,
               e.name as equipment,
               es.stock as stock,
               (es.stock - sum(bed.quantity)) as available,
               sum(bed.quantity) as booked')
            ->from(Station::class, 's')
            ->innerJoin('s.bookings', 'b')
            ->innerJoin('b.equipmentDetails', 'bed')
            ->innerJoin('bed.equipment', 'es')
            ->innerJoin('es.equipment', 'e')
            ->where('s.id = :stationId')
            ->addGroupBy('b.startDate')
            ->addGroupBy('s.id')
            ->addGroupBy('s.name')
            ->addGroupBy('es.id')
            ->setParameter('stationId', $station->getId());

        $result = $builder->getQuery()->getArrayResult();

        return $this->groupArrayByDate('startDate', $result);
    }

    /**
     * Function that groups an array of associative arrays by some key.
     *
     * @param {String} $key Property to sort by.
     * @param {Array} $data Array that stores multiple associative arrays.
     * @return array
     */
    private function groupArrayByDate($key, $data)
    {
        $result = [];

        foreach ($data as $val) {
            $date = $val[$key]->format('d/m/Y');

            if(!array_key_exists($date, $result)) {
                $result[$date] = [];
            }

            unset($val[$key]);
            $result[$date][] = $val;
        }

        return $result;
    }

}
