<?php

namespace App\Repository;

use App\Entity\EquipmentStock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipmentStock|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipmentStock|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipmentStock[]    findAll()
 * @method EquipmentStock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentStockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentStock::class);
    }

}
