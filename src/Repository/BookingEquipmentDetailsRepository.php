<?php

namespace App\Repository;

use App\Entity\BookingEquipmentDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookingEquipmentDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingEquipmentDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingEquipmentDetails[]    findAll()
 * @method BookingEquipmentDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingEquipmentDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookingEquipmentDetails::class);
    }

}
