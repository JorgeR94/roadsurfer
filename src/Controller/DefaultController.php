<?php


namespace App\Controller;


use App\Service\BookingService;
use App\Service\StationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    protected StationService $stationService;
    protected BookingService $bookingService;

    public function __construct(
        StationService $stationService,
        BookingService $bookingService
    )
    {
        $this->stationService = $stationService;
        $this->bookingService = $bookingService;
    }

    public function index(): Response
    {
        $stations = $this->stationService->findAll();

        $dashboard = [];

        foreach($stations as $station) {
            $dashboard[] = [
                'station' => $station->getName(),
                'calendar' => $this->bookingService->getDashboardCalendarByStation($station)
            ];
        }

        return $this->render('base.html.twig', [
            'dashboard' => $dashboard
        ]);
    }
}